/* global src */

$(document).ready(function () {
  $('#Container').mixItUp();

  new WOW().init();

  $('.owl-carousel#sync1').owlCarousel({
    items:1,
    loop:true,
    nav: true,
    navText: ['<i class="fa fa-angle-left"><i>', '<i class="fa fa-angle-right"><i>'],
    URLhashListener:true,
    autoplayHoverPause:true,
    startPosition: 'URLHash'
  });





});
